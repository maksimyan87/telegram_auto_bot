import os
from emoji import emojize

TOKEN = '6123362442:AAEEdbiEjXWP2q0k1SkSSJdpezejSKSCsYI'
NAME_DB = 'products.db'
VERSION = '0.0.1'
AUTHOR = 'User'

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
DATABASE = os.path.join('sqlite:///'+BASE_DIR, NAME_DB)

COUNT = 0

KEYBOARD = {
    'CHOOSE_GOODS': emojize(':open_file_folder: Выбрать товар'),
    'INFO': emojize(':speech_balloon: О магазине'),
    'SETTINGS': emojize('⚙️ Настройки'),
    'CarBody': emojize(':automobile: Кузов'),
    'RunningGear': emojize(':wheel: Ходовая'),
    'CarEngine': emojize('Двигатель'),
    '<<': emojize('⏪'),
    '>>': emojize('⏩'),
    'BACK_STEP': emojize('◀️'),
    'NEXT_STEP': emojize('▶️'),
    'ORDER': emojize('✅ ЗАКАЗ'),
    'X': emojize('❌'),
    'DOWN': emojize('🔽'),
    'AMOUNT_PRODUCT': COUNT,
    'AMOUNT_ORDERS': COUNT,
    'UP': emojize('🔼'),
    'APPLAY': '✅ Оформить заказ',
    'COPY': '©️'
}


CATEGORY = {
    'CarBody': 1,
    'RunningGear': 2,
    'CarEngine': 3,
}

COMMANDS = {
    'START': "start",
    'HELP': "help",
}