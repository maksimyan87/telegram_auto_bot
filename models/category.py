from sqlalchemy import Column, String, Integer, Boolean
from sqlalchemy.ext.declarative import declarative_base
from data_base.dbcore import Base


class Category(Base):
    # name table
    __tablename__ = 'category'

    # fields table
    id = Column(Integer, primary_key=True)
    name = Column(String, index=True)
    is_active = Column(Boolean)

    def __str__(self):

        return self.name